# Authors

## Written by

* [EGLEO](https://github.com/EGLEO) - __Antonio Leonardo G.__ <<antoniognull@proton.me>> (he/they)

## Supporters

* __Nox__ - [@Redhairedpunky](https://twitter.com/i/user/3036415540) (she/they)
* __Naomi__ - [@MissNaaomi](https://twitter.com/i/user/777179627395645440) (she/her)
* __Ash__ - [@PUNKC0WBOY](https://twitter.com/i/user/752811584150499328) (he/they)
* __Haru__ - [@HarukiNB](https://twitter.com/i/user/217120272) (they/them)
* __Aza__ - [@oozorahiros](https://twitter.com/i/user/1634674277529034753) (she/he/they)
* __Kaatios__ - [@Kaatios](https://twitter.com/i/user/1201895286144217088) (they/he)
