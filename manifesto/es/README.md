# Manifiesto de Haven Project

Haven Project es una iniciativa surgida en el año 2023. Nuestro objetivo principal es construir una red social que sirva como alternativa a la catastrófica experiencia en que se ha convertido Twitter / X.

Si bien ya existen opciones, como [Mastodon](https://joinmastodon.org) o [Bluesky](https://bsky.app), que buscan suplir esta necesidad, el propósito de Haven Project no es competir con ninguno de estos proyectos. Lo que buscamos es enriquecer con nuestra visión el ecosistema de redes sociales de microblogging, en contraposición a la hegemonía que hasta hace poco ha mantenido Twitter.

Desde Haven Project creemos que los usuarios deberían elegir una u otra red social según sus funcionalidades, y el interés que tengan en ellas. En los últimos años hemos observado una cierta tendencia por parte de las grandes redes sociales de añadir funcionalidades de sus competidoras. Por ejemplo, ante el éxito de Snapchat, Instagram implementó en 2016 una funcionalidad de historias; con el tiempo, otras plataformas como WhatsApp o incluso Twitter hicieron lo propio. Para referirnos a este fenómeno, hemos acuñado el término **homogeneización de funcionalidades**.

Frente a esta homogeneización de funcionalidades, en Haven Project defendemos que una plataforma de microblogging, como cualquier otra red social, debería contar solo con aquellas funcionalidades que resulten útiles a los usuarios. Nuestra estrategia no es intentar captar a toda costa a los usuarios descontentos de Twitter, sino crear una plataforma con una filosofía totalmente diferente que pueda alinearse mejor con los intereses de ciertos tipos de usuarios.

## El desastre de Twitter

La compra por parte de Elon Musk de la red social anteriormente conocida como Twitter marca un antes y un después en la decadencia de esta plataforma. En el último año, hemos presenciado todo tipo de decisiones corporativas cuestionables y prácticas injustificables. Desde la eliminación del sistema clásico de verificación de cuentas hasta el catastrófico intento de *rebranding* de Twitter a X, pasando por una excesiva permisividad ante el aumento de los discursos de odio, lo cierto es que la experiencia de usuario ha empeorado de manera preocupante.

Sabemos que ni siquiera los gigantes del sector de las TIC son intocables. Skype y Myspace son ejemplos de proyectos que otrora gozaron de un éxito que no parecía tener límites y que, con el tiempo, cayeron en la irrelevancia. Creemos que tal puede ser el destino de Twitter en un futuro no muy lejano. De hecho, creemos que la caída de Twitter podría ser la chispa que encienda el fuego del cambio de paradigma en el mundo de las redes sociales.

## La filosofía de Haven Project

La filosofía de nuestro proyecto está cimentada en tres pilares fundamentales:

1. **Cooperación en lugar de competencia**. Tal y como se ha dicho antes, Haven Project no busca competir con otras plataformas que se presentan como alternativa a Twitter / X. Estamos dispuestos a colaborar con otros proyectos de red social si con ello logramos construir un espacio más constructivo y diverso.

2. **Comunidad en lugar de negocio**. Haven Project se compromete a priorizar a los usuarios y a las comunidades que puedan surgir dentro de nuestra plataforma frente a intereses comerciales. Asimismo, Haven Project se compromete a implementar una política estricta contra los discursos de odio, la desinformación y el acoso, y a destinar los recursos que sean necesarios para mantener un equipo de moderación que vele por el cumplimiento de estas normas.

3. **Creatividad en lugar de monopolio**. Creemos que limitar ciertas funcionalidades de una red social a los usuarios "premium" perjudica no solo la experiencia de usuario, sino también a la propia red social. Asimismo, creemos que la creatividad y el ingenio de los usuarios son fundamentales para el éxito de una red social. Por ello, Haven Project se compromete a dar a sus usuarios las herramientas para explorar esa creatividad, manteniendo todas las funcionalidades de la plataforma disponibles para todos los usuarios.

## Conclusiones

Sabemos que crear una red social no es tarea fácil. Sabemos que un proyecto de esta envergadura requiere un nivel de apoyo con el que a día de hoy no contamos. Sin embargo, tenemos la esperanza de que este manifiesto, así como los avances en el desarrollo de Haven que están por venir, ayuden a dar a conocer nuestra propuesta.
