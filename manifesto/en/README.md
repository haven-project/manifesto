# Haven Project manifesto

Haven Project is an initiative created in 2023. Our main goal is to create a social network that serves as an alternative to the catastrophic experience that Twitter / X has become.

While there's already other options, [Mastodon](https://joinmastodon.org) or [Bluesky](https://bsky.app), that seek to cover this need, Haven Project does not intend to compete with any of these projects. Rather, we aim to broaden with our vision this ecosystem of microblogging social networks, in contrast to the hegemony that Twitter has maintained until recently.

The Haven Project team believes that users should get to choose a social network depending on their interest in its features. In the last few years, we've observed a certain tendency for big social networks to add features from their competition. As an example, upon the success of Snapchat, Instagram implemented story functionalities. Over time, other platforms such as WhatsApp or even Twitter did the same. In order to refer to this phenomenon, we've coined the term **feature homogenization**.

As opposed to this feature homogenization, Haven Project upholds that any microblogging platform, like any other type of social network, should only implement those features that are valuable for the user. Our strategy is not about attracting malcontent Twitter users, but about creating a new platform with a totally different philosophy that might align better with the interests of certain types of users.

## The Twitter fiasco

The acquisition of the social network formerly known as Twitter by Elon Musk marks a turning point in this platform's decadence. Throughout this year, we've witnessed all sorts of questionable business decisions and unjustifiable practices. From the removal of the classic account verification system and the catastrophic attempt to rebrand Twitter to X, through the excessive tolerance towards hate speech, the truth is that the user experience has deteriorated to a concerning extent.

We know for a fact that not even the IT giants are untouchable. Skype and Myspace are two examples of projects that once enjoyed a seemingly unlimited level of success and, over time, crumbled into irrelevance. We believe that such may be Twitter's destiny in the not too distant future. In fact, we believe that Twitter's downfall could be the spark that lights the fire of a paradigm shift in the world of social networks.

## Haven Project's philosophy

Our project's philosophy is built on three fundamental cornerstones:

1. **Cooperation instead of competition**. As mentioned before, Haven Project does not seek to compete with other platforms that present themselves as an alternative to Twitter / X. We're willing to collaborate with other projects if that helps build a more constructive, diverse space.

2. **Community instead of business**. Haven Project is fully committed to prioritizing our users and communities over commercial interest. We're also fully committed to implementing strict policies against hate speech, disinformation and harassment, and we will allocate all the necessary resources to maintain a moderation team that enforces these rules.

3. **Creativity instead of monopoly**. We believe that a social network limiting some of its features to "premium" users does not only harm the user experience, but also the social network itself. We also believe that creativity and imagination are crucial to the success of a social network. Therefore, Haven Project commits itself to offering the users all the tools to explore that creativity, keeping all its features available to all users.

## Conclusions

We are aware that creating a social network is no easy task. We know that a project of this scope requires an amount of support that we haven't found so far. Nevertheless, we hope that this manifesto, along with the upcoming progress in the development of Haven, will help us promote our endeavor.
