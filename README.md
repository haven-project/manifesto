# The Haven Project Manifesto

This repository contains the Haven Project manifesto in the following languages:

- [English](manifesto/en/README.md)
- [Spanish](manifesto/es/README.md)

## License

(C) 2023 The Haven Project and other collaborators. For the full list of contributors and supporters, please refer to [AUTHORS.md](AUTHORS.md).

The Haven Project manifesto is released under the Creative Commons license [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Please refer to [LICENSE.txt](LICENSE.txt) for the full license text.
